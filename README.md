# Simple dockerized relay-only MTA with CLI

You need a simple relay-only mail transport agent for sending emails in your scripts? Maybe this Docker image will fit your needs. It makes use of [nullmailer][1].

I took parts of the `Dockerfile` from [vimagick][2] and added the configuration via environment variables, volume support and running as non-root.

## docker-compose

An example `docker-compose.yml` for Gmail:

```
version: '3'

services:
    nullmailer:
        build:
            context: ./nullmailer
            args:
                SMTP_HOST: smtp.gmail.com
                SMTP_PORT: 587
                SMTP_USER: user@gmail.com
                SMTP_PASSWORD: 123456
                SMTP_SSL: starttls
                REPLY_TO: user@example.com
                FROM_DOMAIN: example.com
        container_name: nullmailer
        restart: unless-stopped
        volumes:
            - srv:/srv

volumes:
    srv:
```

## Usage

Nullmailer does not expose a SMTP port, you can only use it by its command line interface `sendmail`:

```
$ alias sendmail='docker exec -i nullmailer sendmail'
$ echo -e 'Subject: Hello\r\n\r\nHello world' | sendmail user@example.com
```

NB: For a relay-only MTA with an exposed SMTP port I have a [different Docker image](https://github.com/sniner/docker-smtp-satellite) using `postfix`.


[1]: http://untroubled.org/nullmailer/
[2]: https://github.com/vimagick/dockerfiles/tree/master/nullmailer
[3]: http://wiki.leupers.net/index.php?title=Nullmailer
[4]: http://www.troubleshooters.com/linux/nullmailer/
[5]: http://www.troubleshooters.com/linux/nullmailer/landmines.htm
