#!/bin/bash

CONF_DIR=/srv/nullmailer
REMOTES_CONF=${CONF_DIR}/remotes

if [[ -z "${SMTP_SSL}" ]]; then
    case "${SMTP_PORT}" in
        465)
            SSL_OPT=" --ssl"
            ;;
        587)
            SSL_OPT=" --starttls"
            ;;
        *)
            SSL_OPT=""
            ;;
    esac
else
    SSL_OPT=" --${SMTP_SSL,,}"
fi

umask 077

echo "${FROM_DOMAIN:-localdomain}" > ${CONF_DIR}/defaulthost
echo "${REPLY_TO:-do-not-reply@localhost}" > ${CONF_DIR}/adminaddr
echo -n "${SMTP_HOST:-smtp.gmail.com} smtp" > ${REMOTES_CONF}
echo -n " --port=${SMTP_PORT:-465}" >> ${REMOTES_CONF}
if [[ -n "${SMTP_LOGIN}" ]]; then
    echo -n " --auth-login" >> ${REMOTES_CONF}
fi
if [[ -n "${SMTP_USER}" ]]; then
    echo -n " --user=${SMTP_USER}" >> ${REMOTES_CONF}
fi
if [[ -n "${SMTP_PASSWORD}" ]]; then
    echo -n " --pass=${SMTP_PASSWORD}" >> ${REMOTES_CONF}
fi
echo "${SSL_OPT}" >> ${REMOTES_CONF}

exec nullmailer-send
